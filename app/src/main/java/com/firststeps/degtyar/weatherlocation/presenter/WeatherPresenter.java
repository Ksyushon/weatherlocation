package com.firststeps.degtyar.weatherlocation.presenter;
import android.util.Log;

import com.firststeps.degtyar.weatherlocation.model.Clouds;
import com.firststeps.degtyar.weatherlocation.model.Coord;
import com.firststeps.degtyar.weatherlocation.model.Main;
import com.firststeps.degtyar.weatherlocation.model.Rain;
import com.firststeps.degtyar.weatherlocation.model.Sys;
import com.firststeps.degtyar.weatherlocation.model.Weather;
import com.firststeps.degtyar.weatherlocation.model.WeatherModel;
import com.firststeps.degtyar.weatherlocation.model.Wind;
import com.firststeps.degtyar.weatherlocation.service.WeatherService;
import com.firststeps.degtyar.weatherlocation.view.MainActivity;
import com.firststeps.degtyar.weatherlocation.view.fragments.WeatherFragment;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class WeatherPresenter {


    private MainActivity mainActivity;
    private WeatherService weatherService;

    public WeatherPresenter( WeatherService weatherService, MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        this.weatherService = weatherService;
    }

    public void getWeatherInfo(String cityName, String appid) {


        weatherService.getApi()
                .getWeatherByCity(
                        cityName,
                        appid,
                        "metric",
                         Locale.getDefault().getLanguage(),

                        new Callback<Response>() {
                            @Override
                            public void success(Response response, Response response2) {

                                BufferedReader reader = null;
                                StringBuilder sb = new StringBuilder();
                                try {
                                    reader = new BufferedReader(new InputStreamReader(response.getBody().in()));
                                    String line;
                                    while ((line = reader.readLine()) != null) {
                                        sb.append(line);
                                    }
                                      Log.d("tag", sb.toString());
                                  parseWeatherInfo(sb.toString());

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void failure(RetrofitError retrofitError) {
                                Log.d("tag", "error: " + retrofitError.toString() + " ");

                            }
                        });


    }

    public void parseWeatherInfo(String responseString) {

        try {

            JSONObject response = new JSONObject(responseString);
            WeatherModel model = new WeatherModel();

            model.setBase(response.getString("base"));
            model.setCod(response.getString("cod"));
            model.setDt(response.getString("dt"));
            model.setId(response.getString("id"));
            model.setName(response.getString("name"));

            JSONObject coordObject = response.getJSONObject("coord"); //Coord
            Coord coord = new Coord();
            coord.setLat(coordObject.getString("lat"));
            coord.setLon(coordObject.getString("lon"));

            JSONObject mainObject = response.getJSONObject("main"); //Main
            Main main = new Main();
            main.setHumidity(mainObject.getString("humidity"));
            main.setPressure(mainObject.getString("pressure"));
            main.setTemp(mainObject.getString("temp"));
            main.setTemp_max(mainObject.getString("temp_max"));
            main.setTemp_min(mainObject.getString("temp_min"));

            JSONObject windObject = response.getJSONObject("wind"); //Wind
            Wind wind = new Wind();
            wind.setDeg(windObject.getString("deg"));
            wind.setSpeed(windObject.getString("speed"));

            JSONObject cloudsObject = response.getJSONObject("clouds"); //Cloud
            Clouds cloud = new Clouds();
            cloud.setAll(cloudsObject.getString("all"));

            JSONObject sysObject = response.getJSONObject("sys"); //Sys
            Sys sys = new Sys();

            sys.setCountry(sysObject.getString("country"));
            sys.setMessage(sysObject.getString("message"));
            sys.setSunrise(sysObject.getString("sunrise"));
            sys.setSunset(sysObject.getString("sunset"));

            JSONArray weatherArray=response.getJSONArray("weather"); //Weather

            Weather[] weather=new Weather[weatherArray.length()];

            for (int i = 0; i < weatherArray.length(); i++) {

                JSONObject weatherObject = weatherArray.getJSONObject(i);
                Weather weatherElem = new Weather();
                weatherElem.setId(weatherObject.getString("id"));
                weatherElem.setDescription(weatherObject.getString("description"));
                weatherElem.setIcon(weatherObject.getString("icon"));
                weatherElem.setMain(weatherObject.getString("main"));
                weather[i]=weatherElem;
            }

            model.setCoord(coord);
            model.setMain(main);
            model.setWind(wind);
            model.setClouds(cloud);
            model.setSys(sys);
            model.setWeather(weather);

            mainActivity.setGlobalModel(model);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}
