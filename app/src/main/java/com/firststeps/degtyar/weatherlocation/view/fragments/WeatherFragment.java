package com.firststeps.degtyar.weatherlocation.view.fragments;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.firststeps.degtyar.weatherlocation.ConvertUtils;
import com.firststeps.degtyar.weatherlocation.view.MainActivity;
import com.firststeps.degtyar.weatherlocation.R;
import com.firststeps.degtyar.weatherlocation.model.WeatherModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class WeatherFragment extends Fragment {

    MainActivity activity;
    private View view;

    @Bind(R.id.textViewToday)
    public TextView today;

    @Bind(R.id.textViewCity)
    public TextView city;

    @Bind(R.id.textViewCloudiness)
    public TextView cloudiness;

    @Bind(R.id.textViewDescription)
    public TextView description;

    @Bind(R.id.textViewHumidity)
    public TextView humidity;

    @Bind(R.id.textViewMinMaxTemp)
    public TextView minMaxTemp;

    @Bind(R.id.textViewPressure)
    public TextView pressure;

    @Bind(R.id.textViewWindSpeed)
    public TextView windSpeed;

    @Bind(R.id.textViewTemp)
    public TextView temp;

    @Bind(R.id.textViewSunrise)
    public TextView sunrise;

    @Bind(R.id.textViewSunset)
    public TextView sunset;

    @Bind(R.id.textViewUpdated)
    public TextView updated;

    @Bind(R.id.imageViewWeatherState)
    public ImageView weatherState;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.weather_fragment, container, false);
        ButterKnife.bind(this, view);
        today.setText(ConvertUtils.getDateTime(true));
        return view;
    }

    public void bindControls(WeatherModel model) {

        city.setText(model.getName() + ", " + model.getSys().getCountry());
        description.setText(model.getWeather()[0].getDescription());
        humidity.setText(activity.getString(R.string.humidity) + model.getMain().getHumidity() + "%");
        temp.setText(model.getMain().getTemp() + "°C");
        minMaxTemp.setText(activity.getString(R.string.min_temp) +model.getMain().getTemp_min() + "°C"+activity.getString(R.string.max_temp) + model.getMain().getTemp_max() + "°C");
        pressure.setText(activity.getString(R.string.pressure) + model.getMain().getPressure() + activity.getString(R.string.hpa));
        windSpeed.setText(activity.getString(R.string.wind_speed) + model.getWind().getSpeed() + activity.getString(R.string.metr_sec));
        cloudiness.setText(activity.getString(R.string.cloud) + model.getClouds().getAll() + "%");
        sunrise.setText(activity.getString(R.string.sunrise) + ConvertUtils.convertUnixTimeToLocal(model.getSys().getSunrise()));
        sunset.setText(activity.getString(R.string.sunset) + ConvertUtils.convertUnixTimeToLocal(model.getSys().getSunset()));
        updated.setText(activity.getString(R.string.last_update)+ConvertUtils.getDateTime(false));

        Glide.with(this)
                .load(activity.ROOT_URL + "img/w/" + model.getWeather()[0].getIcon() + ".png")
                .into(weatherState);
    }

    @OnClick(R.id.imageViewRenew)
    public void updateForecast() {
        String searchCity=city.getText().toString();
        activity.runService(searchCity);
      }




}
