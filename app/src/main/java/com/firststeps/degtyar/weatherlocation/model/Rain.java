package com.firststeps.degtyar.weatherlocation.model;

/**
 * Created by Ksyu on 26.01.2016.
 */

public class Rain {

    private String _1h;

    public String get1h() {
        return _1h;
    }

    public void set1h(String _1h) {
        this._1h = _1h;
    }

    @Override
    public String toString() {
        return "ClassPojo [1h = " + _1h + "]";
    }
}

