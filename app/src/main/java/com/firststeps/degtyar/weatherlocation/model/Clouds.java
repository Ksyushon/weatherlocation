package com.firststeps.degtyar.weatherlocation.model;

/**
 * Created by Ksyu on 26.01.2016.
 */

public class Clouds {
    private String all;

    public String getAll() {
        return all;
    }

    public void setAll(String all) {
        this.all = all;
    }

    @Override
    public String toString() {
        return "ClassPojo [all = " + all + "]";
    }
}

