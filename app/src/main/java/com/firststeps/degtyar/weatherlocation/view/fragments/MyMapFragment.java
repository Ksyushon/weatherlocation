package com.firststeps.degtyar.weatherlocation.view.fragments;

//import android.app.Fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firststeps.degtyar.weatherlocation.view.MainActivity;
import com.firststeps.degtyar.weatherlocation.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class MyMapFragment extends Fragment implements OnMapReadyCallback {

    private MainActivity activity;
    private GoogleMap mMap;
    private SupportMapFragment mSupportMapFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.map_fragment, container, false);
        ButterKnife.bind(this, v);

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        boolean mapAvailable = googleAPI.isGooglePlayServicesAvailable(activity.getApplicationContext()) == ConnectionResult.SUCCESS;

        if (mapAvailable) {

            mSupportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.mapView);
            if (mSupportMapFragment == null) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                mSupportMapFragment = SupportMapFragment.newInstance();
                fragmentTransaction.replace(R.id.mapView, mSupportMapFragment).commit();
                mSupportMapFragment.getMapAsync(this);
            }
        }

        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);
    }

    //Add marker to map
    public void setCityLocation(double lat, double lon, String title, String temp) {
        if (mMap!=null) {
            LatLng location = new LatLng(lat, lon);
            mMap.addMarker(new MarkerOptions().position(location).title(title + " " + temp + "°C"));
            mMap.animateCamera(CameraUpdateFactory.newLatLng(location));
        }
    }

    //Clear map
    @OnClick(R.id.buttonClear)
    public void clearMap(){
      mMap.clear();
    }

}
