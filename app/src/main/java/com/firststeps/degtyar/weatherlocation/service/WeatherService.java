package com.firststeps.degtyar.weatherlocation.service;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class WeatherService {


    private WeatherApi weatherApi;

    public WeatherService(String ROOT_URL) {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL)
                .build();
        weatherApi = restAdapter.create(WeatherApi.class);
    }

    public WeatherApi getApi() {
        return weatherApi;
    }

    public interface WeatherApi {

        @POST("/data/2.5/weather")
        public void getWeatherByCity(
                @Query("q") String cityName,
                @Query("appid") String appid,
                @Query("units")String units,
                @Query("lang")String language,
                Callback<Response> callback);

    }

}
