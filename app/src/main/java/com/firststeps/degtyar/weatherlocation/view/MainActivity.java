package com.firststeps.degtyar.weatherlocation.view;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.firststeps.degtyar.weatherlocation.ConvertUtils;
import com.firststeps.degtyar.weatherlocation.R;
import com.firststeps.degtyar.weatherlocation.adapter.MyViewPagerAdapter;
import com.firststeps.degtyar.weatherlocation.model.WeatherModel;
import com.firststeps.degtyar.weatherlocation.presenter.WeatherPresenter;
import com.firststeps.degtyar.weatherlocation.service.WeatherService;
import com.firststeps.degtyar.weatherlocation.tabs.SlidingTabLayout;
import com.firststeps.degtyar.weatherlocation.view.fragments.MyMapFragment;
import com.firststeps.degtyar.weatherlocation.view.fragments.WeatherFragment;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    public static String ROOT_URL = "http://api.openweathermap.org/";

    ViewPager pager;
    SlidingTabLayout tabs;
    WeatherPresenter weatherPresenter;
    WeatherService weatherService;

    WeatherFragment weatherFragment;
    MyMapFragment mapFragment;

    SearchView searchView;

    public WeatherModel model;
    public MyViewPagerAdapter viewPagerAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        pager = (ViewPager) findViewById(R.id.pager);
        viewPagerAdapter = new MyViewPagerAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(viewPagerAdapter);
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);
        tabs.setViewPager(pager);

        weatherService = new WeatherService(ROOT_URL);
        weatherPresenter = new WeatherPresenter(weatherService, this);
        weatherFragment = (WeatherFragment) viewPagerAdapter.getItem(0);
        mapFragment = (MyMapFragment) viewPagerAdapter.getItem(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.search_item).getActionView();
        int searchIconId = searchView.getContext().getResources().getIdentifier(
                "android:id/search_button", null, null);

        SearchView.OnQueryTextListener queryTextListener = new SearchView.OnQueryTextListener() {
            StringBuilder searchString = new StringBuilder();

            @Override
            public boolean onQueryTextChange(String newText) {
                searchString.setLength(0);
                searchString.append(newText);
                return true;
            }

            @Override
            public boolean onQueryTextSubmit(String query) {
                View view = getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm
                            = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                Log.d("tag", "query - " + query);

                if (pager.getCurrentItem() != 0) {
                    pager.setCurrentItem(0);
                }

                runService(query);

                return true;
            }
        };
        searchView.setOnQueryTextListener(queryTextListener);
        return true;
    }

    public void runService(String query) {

        if (isOnline(getApplicationContext())) {
            weatherPresenter.getWeatherInfo(query, getString(R.string.app_id));
        }else{

            Toast.makeText(this, R.string.inet_connection, Toast.LENGTH_LONG).show();
        }
    }

    public void setGlobalModel(WeatherModel model) {
        weatherFragment.bindControls(model);
        mapFragment.setCityLocation(Double.parseDouble(model.getCoord().getLat()), Double.parseDouble(model.getCoord().getLon()),
                model.getName(), String.valueOf(model.getMain().getTemp()));
    }

    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting())
        {
            return true;
        }
        return false;
    }

}
