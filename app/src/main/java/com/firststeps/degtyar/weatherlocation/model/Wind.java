package com.firststeps.degtyar.weatherlocation.model;

/**
 * Created by Ksyu on 26.01.2016.
 */

public class Wind {


    private String speed;

    private String deg;


    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getDeg() {
        return deg;
    }

    public void setDeg(String deg) {
        this.deg = deg;
    }

    @Override
    public String toString() {
        return "ClassPojo [speed = " + speed + ", deg = " + deg + "]";
    }
}