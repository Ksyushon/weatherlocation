package com.firststeps.degtyar.weatherlocation.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.firststeps.degtyar.weatherlocation.view.MainActivity;
import com.firststeps.degtyar.weatherlocation.R;
import com.firststeps.degtyar.weatherlocation.view.fragments.MyMapFragment;
import com.firststeps.degtyar.weatherlocation.view.fragments.WeatherFragment;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class MyViewPagerAdapter extends FragmentPagerAdapter {

    String[] tabs;
    MainActivity activity;

    WeatherFragment weatherFragment;
    MyMapFragment mapFragment;

    public MyViewPagerAdapter(FragmentManager fm, MainActivity activity) {
        super(fm);
        this.activity = activity;
        tabs = activity.getResources().getStringArray(R.array.tabs);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position];
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                if (weatherFragment == null) weatherFragment = new WeatherFragment();
                return weatherFragment;

            case 1:
                if (mapFragment == null) mapFragment = new MyMapFragment();
                return mapFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return tabs.length;
    }
}
