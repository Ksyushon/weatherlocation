package com.firststeps.degtyar.weatherlocation;

/**
 * Created by Ksyu on 30.01.2016.
 */

import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by Ksyu on 30.01.2016.
 */
public class ConvertUtils {

    public static String convertUnixTimeToLocal(String unixTime) {

        String parsedTime;
        long unixTimestamp = Long.parseLong(unixTime);
        long javaTimestamp = unixTimestamp * 1000L;
        Date localDate = new Date(javaTimestamp);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm");
        parsedTime = dateFormat.format(localDate);
        return parsedTime;

    }

    public static String getDateTime(boolean returnOnlyDate){

        if(returnOnlyDate){
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy");
            Date today = new Date();
            return dateFormat.format(today);
        }else {
            DateFormat dateFormat = new SimpleDateFormat("dd.MM.yy HH:mm");
            Calendar cal = Calendar.getInstance();
            return dateFormat.format(cal.getTime());
        }

    }

}